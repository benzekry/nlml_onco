This file provides a detailed description of the use cases and the tasks that can be performed. The file settings corresponding to each tasks can be settled by checking out previous commits.

The main file is `main_monotherapy.R`

# Note

Due to legal constraints and the nature of the data (sensitive health data), the data used in this project is not publicly available. However, the code is provided as a demonstration of the workflow and can be adapted to other datasets.

# Train set (no_oak)
## NLME
### Population fit
Tasks:
1. Preprocess of the data (TK and pd)
2. NLME fit on the longitudinal data (TK and pd)

Setup:  
- in `load_settings.R` : set
  - `study_times = NULL` (no randomization dates provided for the train set).
  - `cycle_horizons = NULL` (no cycle truncation)
- use `no_oak` as dataset_name
- uncomment/comment the desired tasks in `tasks_xx` variables
  - "preprocess"
  - "fit_nlme"

Outputs:
- In `TK_monolix/` or `pd_monolix/`
  - `files/` : preprocessed files
  - `no_oak` : NLME files. Monolix project and associated outputs
- In `clinical/no_oak`: build of clinical file

### Bayesian estimation
Tasks:
1. Empirical Bayesian estimates (TK and pd). A priori distribution used from the train set.
2. Export of the individual parameters for ML

Setup:  
- use `no_oak` as dataset_name
- uncomment/comment the desired tasks in `tasks_xx` variables  
  - "fit_bayesian"
  - "export"

Outputs:
- In `TK_monolix/` or `pd_monolix/`
  - `files/` : preprocessed files
  - `oak_bayesian` : NLME files. Monolix project and associated outputs, including empirical Bayes estimates
- In `features_outcome/oak` : EBEs (`tk` and `pd_model`)
- In `clinical/oak`: build of clinical file

### Cycle-truncation
Tasks:
1. Empirical Bayes estimates of model-parameters on truncated data from train set. The prior used is the one fitted on the entire (full time courses) dataset. Done for both tk and pd.

Setup:
- in `load_settings`
  - `cycle_horizons = c(3, 5, 10)`

Output:
- in both `TK_monolix` and `pd_monolix`
  - `no_oak_bayesian_truncated/X` with X being the truncated cycle, project files and outputs from Monolix.
- in `features_outcome/no_oak`, subfolders and files corresponding to estimated model parameters are exported

## Machine Learning
### Train
Tasks:
1. Preprocess clinical
2. Build dataset for ML
3. Train the model

Setup:  
`preprocess_clinical_bool = T`
```
tasks_ml = list(
  build_dataset = T,
  run_ml        = T,
  job           = c("train"),
  post_process  = F,
  NULL
)
```

Output:
- `features_outcome/no_oak/clinical.csv` : preprocessed clinical data
- `features_outcome/no_oak/all_features.csv` : data frame with all features
`features_outcome/no_oak/outcome.csv` : outcome data (survival)
- `features_outcome/no_oak/features_names` : post-variable selection sets of features names (including the minimal signature in `clinical_minimal_tk_no_select_pd_model_no_select.txt`)
- `machine_learning/no_oak/fitted_models/clinical_minimal_tk_no_select_pd_model_no_select_transformer_results.rds` : transformer object fitted on the train set. Used in the predict phase to apply same preprocess steps (learned on train) to the test set
- `machine_learning/no_oak/fitted_models/random_survival_forest/clinical_minimal_tk_no_select_pd_model_no_select.rds` : trained ML model

### Cross-validation
Task:  
1. Perform cross-validation

Setup:
```
tasks_ml = list(
  build_dataset = T,
  run_ml        = T,
  job           = c("train"),
  job           = c("cross_val"),
  post_process  = F,
  NULL
)
```

Output:
`code/machine_learning/no_oak/cross_validation_no_tune/random_survival_forest/clinical_minimal_tk_no_select_pd_model_no_select.rds` : prediction results for each cross-validation fold

### Train cycle-truncated models
#### Baseline data (clinical)

Task:
1. Train the model on the baseline data (clinical features)

Setup:
```
features_labels = c(
  "clinical",
  # "tk",
  # "pd_model",
  NULL)
```

Output:
- `machine_learning/no_oak/fitted_models/clinical_minimal_transformer_results.rds` : transformer object
- `machine_learning/no_oak/fitted_models/random_survival_forest/clinical_minimal.rds` : trained ML model on baseline data

#### Cycle-truncated

To launch multiple models, use `main_multiple_features.R`

Task:
1. Train the models on cycle-truncated data

Setup:
```
## truncated cycles
features_labelss_cycle = cycle_horizons %>% purrr::map(
  function(cycle_horizon){
    c("clinical",
      paste0("tk_", cycle_horizon),
      paste0("pd_model_", cycle_horizon))})
```

Output:
- `features_outcome/no_oak/all_features.csv` : dataset containing all features
- `features_outcome/no_oak/features_names/` : names of variables in possible signatures
- `machine_learning/no_oak/fitted_models/` ; trained ML models on cycle-truncated data

#### Study time-truncated
For this task, no need to re-train models on the train set. The full time course (train set) prior is used for Bayesian parameter estimates in the test set.

----

# Test set (oak)
## NLME
### Bayesian estimation
Tasks:
1. Preprocess of the data (TK and pd)
2. Empirical Bayesian estimates (TK and pd). A priori distribution used from the train set.
3. Export of the individual parameters for ML

Setup:
- use `oak` as dataset_name
- uncomment/comment the desired tasks in `tasks_xx` variables
  - "preprocess"
  - "fit_bayesian"
  - "export"

Outputs:
- In `TK_monolix/` or `pd_monolix/`
  - `files/` : preprocessed files
  - `oak_bayesian` : NLME files. Monolix project and associated outputs, including empirical Bayes estimates
- In `features_outcome/oak` : EBEs (`tk` and `pd_model`)
- In `clinical/oak`: build of clinical file

### Cycle-truncation
Tasks:
1. Empirical Bayes estimates of model parameters on cycle-truncated data from test set. The train prior used is the one fitted on the entire (full time courses) dataset. Done for both tk and pd.

Setup:
```
## TK
tasks_tk = c(
   "preprocess",
   # "fit_nlme",
   # "fit_bayesian",
   "fit_bayesian_truncated",
   # "fit_bayesian_study_times",
   "export",
  NULL
)
## PD
tasks_pd = c(
   "preprocess",
   # "fit_nlme",
   # "fit_bayesian",
   "fit_bayesian_truncated",
   # "fit_bayesian_study_times",
   "export",
  NULL
)
```
Output:
- `TK_monolix/oak_bayesian_truncated/` : Bayesian fitted models
- `pd_monolix/oak_bayesian_truncated/` : Bayesian fitted models
- `features_outcome/oak/` : exported model parameters

### Study-truncation
Tasks:
1. Pre-process of the baseline clinical data to generate study-truncated sets including only patients recruited at a given study time landmark.
2. Empirical Bayes estimates of model parameters on study-truncated data from test set. The train prior used is the one fitted on the entire (full time courses) dataset. Done for both tk and pd.

Setup:
- in `load_settings.R`
```
study_times = c(5, 10, 15, 20, 25, 30, 90, 100)*7 # landmark study times for data truncation
```
- in `main_monotherapy.R`
```
# NLME
## TK
tasks_tk = c(
   "preprocess",
   # "fit_nlme",
   # "fit_bayesian",
   # "fit_bayesian_truncated",
   "fit_bayesian_study_times",
   "export",
  NULL
)
## PD
tasks_pd = c(
   "preprocess",
   # "fit_nlme",
   # "fit_bayesian",
   # "fit_bayesian_truncated",
   "fit_bayesian_study_times",
   "export",
  NULL
)
preprocess_clinical_bool = T
tasks_ml = list(
  build_dataset = F,
  run_ml        = F,
  job           = c("predict"),
  post_process  = F,
  NULL
)
```

Outputs:
- `features_outcome/oak/clinical/clinical_st_XX.csv` : pre-processed clinical data for study truncation. XX is study time trucation in days
- `TK_monolix` or `pd_monolix` :
  - `files/oak/study_times/tk_mlx_st_XX.csv` : study truncation pre-processed TK files. Same for pd.
  - `oak_bayesian_study_times/XX/` : NLME files. Monolix project and associated outputs, including empirical Bayes estimates/
- `features_outcome/oak/tk/tk_st_XX.csv` : exported individual parameters files for use in ML

## Machine learning
### Full time course
Tasks:
1. Preprocess clinical
2. Build dataset for ML
3. Predict

Setup:  
`dataset_name = oak`  
`preprocess_clinical_bool = T`
```
tasks_ml = list(
  build_dataset = T,
  run_ml        = T,
  job           = c("predict"),
  post_process  = F,
  NULL
)
```
Output:
- `features_outcome/oak/clinical.csv` : preprocessed clinical data
- `features_outcome/oak/all_features.csv` : data frame with all features
`features_outcome/oak/outcome.csv` : outcome data (survival)
- `features_outcome/oak/features_names` : post-variable selection sets of features names (including the minimal signature in `clinical_minimal_tk_no_select_pd_model_no_select.txt`)
- `machine_learning/oak/fitted_models/clinical_minimal_tk_no_select_pd_model_no_select_transformer_results.rds` : transformer object fitted on the train set. Used in the predict phase to apply same preprocess steps (learned on train) to the test set
- `machine_learning/oak/test_results/random_survival_forest/clinical_minimal_tk_no_select_pd_model_no_select.rds` : prediction object `res`. Original test data used for prediction are in `res$df`. Variables of interest to the prediction are in `res$results`. Predicted individual survival functions are in `res$results$survival`.


### Cycle-truncated models
#### Baseline data (clinical)

Task:
1. Prediction of test set using the baseline data (clinical features)

Setup:
```
dataset_name          = "oak" #

tasks_ml = list(
  build_dataset = F,
  run_ml        = T,
  job           = c("predict"),
  post_process  = T,
  NULL
)

features_labels = c(
  "clinical",
  # "tk",
  # "pd_model",
  NULL)
```

Output:
- `machine_learning/oak/test_results/random_survival_forest/clinical_minimal.rds` : prediction object `res`
- `machine_learning/oak/test_results/random_survival_forest/post_processed_results/clinical_minimal.rds` : post-processed object

#### Truncated data

To launch multiple models, use `main_multiple_features.R`

Task:
1. Outcome prediction from cycle-truncated test data

Setup:
```
## truncated cycles
features_labelss_cycle = cycle_horizons %>% purrr::map(
  function(cycle_horizon){
    c("clinical",
      paste0("tk_", cycle_horizon),
      paste0("pd_model_", cycle_horizon))})
```
### Study-truncated data
Task:
1. Outcome prediction from study-truncated test data

Setup:
```
# Machine learning
tasks_ml = list(
  build_dataset = T,
  run_ml        = T,
  job           = c("predict"),
  post_process  = T,
  NULL
)
```
Output:
- `features_outcome/oak/all_features.csv`: all features required for ML
- `machine_learning/oak/test_results/random_survival_forest/clinical_st_XX_minimal_tk_st_XX_no_select_pd_model_st_XX_no_select.rds` : object from ML prediction at XX study truncation landmark time

### Oak docetaxel
Task:
1. Build and preprocess oak docetaxel
2. Bayesian estimates
3. ML prediction

Setup:
```
dataset_name          = "oak_docetaxel" #
build_dataset         = T
# NLME
## TK
tasks_tk = c(
   "preprocess",
   # "fit_nlme",
   "fit_bayesian",
   # "fit_bayesian_truncated",
   # "fit_bayesian_study_times",
   "export",
  NULL
)
## PD
tasks_pd = c(
   "preprocess",
   # "fit_nlme",
   "fit_bayesian",
   # "fit_bayesian_truncated",
   # "fit_bayesian_study_times",
   "export",
  NULL
)
# Clinical
preprocess_clinical_bool = T
# Machine learning
tasks_ml = list(
  build_dataset = T,
  run_ml        = T,
  job           = c("predict"),
  post_process  = T,
  NULL
)
```

## Post-process
### Full time course
#### Post-process ML
The post-process needs first to be run from `main_monotherapy.R`.

Setup:
```
tasks_ml = list(
  build_dataset = F,
  run_ml        = F,
  job           = c("predict"),
  post_process  = T,
  NULL
)
```

Output:  
`machine_learning/oak/test_results/random_survival_forest/post_processed_results/clinical_minimal_tk_no_select_pd_model_no_select.rds` : object containing post-processed results of ML.

Then the post-process is dealt with scripts and functions in the `post_process` directory. This dir has to be set as working directory.

#### Plots

- Barplots
 - Script: `main_plot_metrics_landmark_times.R`
 - Output: `/code/machine_learning/oak/test_results/figures/summary_scores_metrics_minimal_12_months.png`

- ROC curve, contingency and metrics table
  - Script: `code/post_process/main_plot_ROC_contingency.R`  
  - Output:
    - `code/machine_learning/oak/test_results/figures/ROC_minimal_365_days.png` : ROC curve
    - `code/machine_learning/oak/test_results/figures/contingency_table_minimal_365_days.pptx` : contingency table
    - `code/machine_learning/oak/test_results/figures/metrics_minimal_365_days.pptx` : metrics table

### Cycle-truncation

- Barplot:
 - Script: `post_process/plot_clinical_tk_pd_truncated.R`
 - Output: `code/machine_learning/oak/test_results/figures/summary_scores_truncated.png`

### Study level
- Data and predicted survival curves in OAK two arms
 - Script: `porst_process/main_survival_oak_two_arms.R`
 - Output: `code/machine_learning/oak_docetaxel/test_results/figures/survival_curve_two_arms_PI.png`
