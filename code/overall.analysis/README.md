
# overall.analysis

The main function of overall.analysis is run_all_analysis, and it should be the only function used by the user. The details of the function inputs are given in the documentation.

## Installation

Before installing the package, run the following lines:

```
devtools::install_github(“hadley/devtools”, ref = “f21ca3516c”)
```
Restart the R Session on unload/reload the devtools package.
```
devtools::install_github("IyarLin/survXgboost")
```

Then run (from the parent directory of the package directory) :
`install("overall.analysis")``

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(overall.analysis)
## basic example code
```
