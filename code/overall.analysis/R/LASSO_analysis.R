#' LASSO feature selection
#' 
#' This function performs LASSO feature selection on df_features.
#' 
#' @param df_features dataframe, containing the features
#' @param df_survival dataframe, containing the target
#' @param dir_out string, folder path to the output directory in which the 
#'                feature sets will be saved
#' @param features_label string, label of the features that make df_features up
#' @param model string, either "cox" for survival analysis or "logistic" for 
#'              binary classification. Default is "cox"
#' @param dir_figures string, folder path to the directory in which the 
#'                    figures will be saved
#' @keywords survival aft normal extreme
#' @examples
LASSO_analysis = function(
  df_features,
  df_survival,
  dir_out,
  features_label,
  model = "cox",
  dir_figures
  ){
    dir.create(file.path(dir_figures,
                         features_label), 
               recursive = T, 
               showWarnings = F)
    dir.create(file.path(dir_out), 
               recursive = T, 
               showWarnings = F)
  
  lasso = "LASSO"
  if(model == "logistic"){
    lasso = paste0(lasso, "_auc")
  }
  df_data = df_features %>%
    inner_join(df_survival, by = "ID") %>%
    na.omit
  # fit for various values of lambda
  if(model=="cox"){
    fit = glmnet(df_data %>% select(-ID, -TIME, -STATUS),
                 as.matrix(df_data %>%
                             dplyr::rename(time = TIME, status = STATUS)%>%
                             select(time, status)),
                 family = "cox")
  }
  if(model == "logistic"){
    fit = glmnet(df_data %>% select(-ID, -PROG),
                 df_data$PROG,
                 family      = "binomial",
                 standardize = FALSE,
                 alpha       = 1)
  }
  pdf(file.path(dir_figures,
                features_label, 
                paste0(lasso, "_fit.pdf")),
      width  = 10,
      height = 5)
  plot(fit, label = T)
  dev.off()
  # get list of ordered selected variables
  list_variables_selected = fit$lambda %>%
    map(~ coef(fit, s = .x) %>%
          as.matrix %>%
          as.data.frame(row.names = rownames(.)) %>%
          filter(`1` != 0) %>%
          row.names())
  variables_sorted = c(Reduce(intersect, 
                              list_variables_selected), # in order to make sure we keep variables which were with every possible lambda
                       seq(1, length(list_variables_selected) - 1) %>%
                         map(.f = function(i){
                           setdiff(list_variables_selected[[i + 1]], 
                                   list_variables_selected[[i]])}) %>%
                         unlist %>%
                         unique) # keep only first occurrence of variable (in some (rare) cases, some variable can stop being selected and then being reselected later when decreasing lambda)
  variables_sorted %>% 
    write_lines(file = file.path(dir_out,
                                 paste0(features_label,
                                        "_",
                                        lasso, 
                                        "_sorted.txt")))
  # get best lambda by cross-validation
  set.seed(1)
  if(model == "cox"){
    cvfit = cv.glmnet(data.matrix(select(df_data, -ID, -TIME, -STATUS)),
                      survival::Surv(df_data$TIME, df_data$STATUS),
                      family = "cox",
                      type.measure = "C"
    )
  }
  if(model=="logistic"){
    cvfit = cv.glmnet(data.matrix(df_data %>% select(-ID, -PROG)),
                      as.matrix(df_data$PROG),
                      family = "binomial",
                      type.measure = "auc",
                      standardize = FALSE,
                      alpha = 1)
  }
  pdf(file.path(dir_figures,
                features_label,
                paste0(lasso, "_cvfit.pdf")),
      width  = 10,
      height = 5)
  plot(cvfit)
  dev.off()
  # get list of selected variables at optimal lambda
  all_coefficients = coef(cvfit, s = "lambda.min") %>%
    as.matrix %>%
    as.data.frame(row.names = rownames(.)) %>% 
    filter(row.names(.) != "(Intercept)") 
  variables_selected = all_coefficients %>% 
    filter(`1` != 0) %>%
    rownames
  variables_selected %>% 
    write_lines(file = file.path(dir_out,
                                 paste0(features_label,
                                        "_",
                                        lasso, 
                                        "_selected.txt")))
  # importance
  features_    = all_coefficients %>% row.names()
  importances_ = all_coefficients %>% pull(`1`)
  plot_features_importance(
    features    = features_,
    importances = importances_,
    file_out    = file.path(
      dir_figures,
      features_label,
      "LASSO_feature_importance.png"
      )
    )
}