#' Preprocess of clinical
#'
#' This function preprocesses the clinical dataframe. It dichotomizes some
#' categorical variables and adds the baseline tumor size.
#'
#' @param clinical_path string, file path to the raw clinical dataset
#' @param output_dir string, directory in which the preprocessed dataframe will be saved. Default is NULL
#' @param tk_path string, file path to tk
#' @keywords preprocess clinical
#' @examples
#' WRITE EXAMPLE HERE
#_______________________________________________________________________________
# dichotomizing categorical variables ----
#_______________________________________________________________________________
dichotom_cat_var = function(df){
  # dichotomizing categorical variables such as race, stage, line and nbmetabsl
  # df is a data.frame
  # in race, "1" stays White, "2" becomes Asian and "3" becomes Others, Unknown or missing.
  cat("Dichotomizing categorical variables\n")
  if("race" %in% colnames(df)){
  df$race = plyr::mapvalues(df$race,
                      from = c("3", "2", "4", "5", "6", "7", "-99"),
                      to = c("2", rep("3", 6)))
}
  # in NBMETABSL, nb of mets greater than 3 are merged to "3"
  if("nbmetabsl" %in% colnames(df)){
    to_be_mapped = df$nbmetabsl[df$nbmetabsl > 3] %>% unique()
    df$nbmetabsl = plyr::mapvalues(df$nbmetabsl,
                             from = to_be_mapped[!is.na(to_be_mapped)],
                             to = c(rep("3", length(to_be_mapped[!is.na(to_be_mapped)]))))
  }
  # in HISTYP, 3, 4, 5 and 6 become 0 (non-squamous)
  if("histyp" %in% colnames(df)){
    df$histyp = plyr::mapvalues(df$histyp,
                                   from = c("3", "4", "5", "6"),
                                   to = c(rep("0", 4)))
}
  # in STAGE, mapping IA and IB to 1, IIA and IIB to 2, etc...
  if("stage" %in% colnames(df)){
    df$stage = plyr::mapvalues(df$stage,
                         from = c("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"),
                         to = c(rep("1", 2), rep("2", 2), rep("3", 2), rep("4", 3), NA))
}
  # in LINE, mapping 4th and 5th line to 3rd line
  if("line" %in% colnames(df)){
    df$line = plyr::mapvalues(df$line,
                        from = c("2", "3", "4", "5"),
                        to = rep(">=2",4))
  }
  # group ECOG 1 and 2 together
  if("ecog" %in% colnames(df)){
    df$ecog = plyr::mapvalues(df$ecog,
                              from = c("2", "3", "4"),
                              to   = c("1", "1", "1"))
    df = df %>% dplyr::rename(ecog_1_2 = ecog)
  }
  # transform LESLOC to number of metastatic sites
  if("lesloc" %in% colnames(df)){
    df = df %>%
      mutate(lesloc = lapply(df$lesloc,
                             function(x) nchar(as.character(x))[[1]]) %>%
               unlist())
  }
  return(df)
}
#_______________________________________________________________________________
# adding baseline sum of largest diameters (sld) to clinical ----
#_______________________________________________________________________________
add_baseline_tumor_sld = function(df, tk_path){
  cat("Adding baseline tumor SLD if not present\n")
  if (! ("bsiz" %in% (df %>% names %>% tolower))){
    df_tk = read_csv(tk_path,
                     show_col_types = FALSE)
    return(df %>%
             full_join(df_tk %>%
                         group_by(ID) %>%
                         dplyr::slice(1) %>%
                         rename(stud=STUD, bsiz=BSIZ) %>%
                         select(ID, bsiz), by="ID"))
  }
  else {
    return(df)
  }
}

#_______________________________________________________________________________
# preprocessing of the data ----
#_______________________________________________________________________________
preprocess_clinical = function(
  clinical_path, # filepath to the raw clinical dataset
  output_dir = NULL, # directory in which the preprocessed dataframe will be saved
  tk_path,
  dates_path = NULL,
  study_times = NULL
)
{
  df_clinical = read_csv(clinical_path,
                         show_col_types = FALSE)
  # transforming the column names to lowercase for consistency
  colnames(df_clinical) = tolower(colnames(df_clinical))
  # rename egfr to eGFR because misleading
  if("egfr" %in% colnames(df_clinical)){
    df_clinical = df_clinical %>% dplyr::rename(eGFR=egfr)}
  # preprocessing pipe
  df = df_clinical %>%
    dplyr::rename(ID=id) %>%
    dichotom_cat_var %>%
    add_baseline_tumor_sld(tk_path)
  # save
  if(!is.null(output_dir)){
    dir.create(output_dir, showWarnings = F, recursive = T)
    df %>% write_csv(file.path(
      output_dir,
      "clinical.csv"))
  }
  # generate truncated study times datasets
  if (!is.null(study_times)){
    for (study_time in study_times){
      df %>%
        # retrieve randomization dates
        left_join(read_csv(dates_path, show_col_types = FALSE), by="ID") %>%
        # filter
        filter((`Date/Time of First Study Treatment`) <=
                 (min(`Date/Time of First Study Treatment`, na.rm = TRUE) + lubridate::ddays(study_time))) %>%
        # remove randomization dates
        select(- `Date/Time of First Study Treatment`, - `Date of Randomization`, - `Study Identifier`) %>%
        # rename
        rename_with(~ paste0(.x, "_st_", study_time), .cols=-ID) %>%
        write_csv(file.path(
          output_dir,
          paste0("clinical_st_", study_time, ".csv")))
    }
  }
  return(df)
}
