#' Predict on a test dataset
#'
#' This function processes the predict object given in predict_res. The output
#' is a list containing multiple survival metrics and the predicted survival
#' curve.
#' It was written assuming that the model saved in model_path is a random
#' survival forest one.
#'
#' @param df_features_outcome dataframe
#' @param predict_res predict model object
#' @param file_path_opt_cut_value path to the cross-validated train model where optimal cut point has been computed
#' @param roc_survival_time number, time at which survival should be investigated. Default is 365
#' @keywords post process predict test
#' @examples
#' WRITE EXAMPLE HERE
post_process_predict = function(
  df_features_outcome,
  predict_res,
  file_path_opt_cut_value,
  roc_survival_times = c(365)
  ){
  # load optimal cutpoint value
  if(!file.exists(file_path_opt_cut_value)){
    warning('It would have been better to perform cross-validation on the train dataset in order to compute the optimal cutpoint value for performance evaluation. However, a default value of 0.33 is chosen.')
    opt_cutpoint = 0.33
  } else{
    train_res = readRDS(file_path_opt_cut_value)
    opt_cutpoint = train_res$opt_cut_value_mean
  }
  # survival curve
  rownames(predict_res$survival) = predict_res$xvar$ID
  survival_curve = cbind(
    data.frame(time = predict_res$time.interest),
    as.data.frame(t(predict_res$survival))) %>%
    pivot_longer(cols      = matches(match = "[0-9]+"),
                 names_to  = "ID",
                 values_to = "survival_probability")
  survival_curve$ID = survival_curve$ID %>% as.numeric()
  # c-index
  df_pred_id = tibble(predicted_time = predict_res$predicted,
                      ID = predict_res$xvar$ID) %>%
    filter(ID %in% df_features_outcome$ID)

  p.test = - df_pred_id %>% pull(predicted_time)
  score = eval(rlang::expr(Hmisc::rcorr.cens(
    p.test,
    with(df_features_outcome, survival::Surv(TIME, STATUS)),
    outx = FALSE)))[1]
  # survival metrics
  survival_metrics = roc_survival_times %>% map(.f = function(x) {
    cat(paste0("ROC survival time = ", x, "\n"))
    survival_metrics  = ROC_survival(
      x,
      survival_curve,
      df_features_outcome %>% select(ID, TIME, STATUS),
      only_metrics = TRUE,
      opt_cutpoint)
    return(tibble(
      "roc_survival_time"    = x,
      "auc"                  = survival_metrics$auc,
      "specificity"          = survival_metrics$specificity,
      "sensitivity"          = survival_metrics$sensitivity,
      "ppv"                  = survival_metrics$ppv,
      "npv"                  = survival_metrics$npv,
      "accuracy"             = survival_metrics$accuracy
    ))}) %>% bind_rows()
  # binding the results
  results = list("c-index"         = score,
                 survival_metrics  = survival_metrics,
                 results_all       = survival_curve,
                 "predicted_times" = df_pred_id
                 )
  return(results)
}
