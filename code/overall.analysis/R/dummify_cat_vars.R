#' Dummification of categorical variables
#' 
#' This function dummifies non-binary categorical variables.
#' 
#' @param transformer_results list, containing a dataframe element named "dataframe"
#' @keywords dummification
#' @examples
#' WRITE EXAMPLE HERE
dummify_cat_vars = function(
  transformer_results 
){
  # identifying binary columns because they're already dummified
  binary_cols = transformer_results$dataframe %>% 
    summarise(across(where(is.factor), 
                     ~ n_distinct(., 
                                  na.rm = TRUE))) %>% 
    select(where(~ sum(.) == 2)) %>% 
    colnames
  # identifying the reference level that will be later dropped after dummification,
  # the reference level being defined as the most frequent level
  reference_levels = transformer_results$dataframe %>% 
    summarise(across(where(is.factor), 
                     ~ as.numeric(get_mode(as.character(.))))) %>%
    select(-any_of(binary_cols)) %>%
    as.list
  transformer_results$to_be_dummified    = names(reference_levels)
  transformer_results$reference_levels   = paste(names(reference_levels),
                                                 reference_levels, 
                                             sep = "_")
  # dummifying and dropping the reference columns
  if(!length(transformer_results$to_be_dummified)==0){
    transformer_results$dataframe = transformer_results$dataframe %>%
      fastDummies::dummy_cols(select_columns = transformer_results$to_be_dummified,
                              remove_selected_columns = TRUE) %>%
      select(-any_of(transformer_results$reference_levels)) %>%
      clean_var_types
  }
  return(transformer_results)
}
