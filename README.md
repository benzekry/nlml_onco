# NLML_ONCO: Nonlinear mixed-effects modeling and machine learning for oncology

This code is intended to be companion to the paper "Predicting survival in patients with advanced NSCLC treated with atezolizumab using pre- and on-treatment prognostic biomarkers", currently under review. 

Note that due to legal constraints and the nature of the data (sensitive health data), the data used in this project is not publicly available. Therefore, the scripts will not be executable as is. It can nevertheless be run on user-defined data, to be placed in the `data` directory and linked to a `build_<dataset>` function.

The code is provided here as a demonstration of the workflow used for model development and validation in the paper. R scripts generating the figures and tables in the paper are provided in the `figures_tables` directory.

Details about how to run specific use cases is given in the `use_cases.md` file.

# Context 
This software analyses multiple data arising from clinical oncology (routine care and clinical trials). This data can be of two types:  
1) static (e.g., baseline features), from clinical, biological, molecular (e.g., transcriptomic or mutation data)  
2) longitudinal (multiple time points per individual): tumor kinetics, biomarkers  

The second type is modeled using the framework of nonlinear mixed-effects modeling, performed using the Monolix R api. Thus, Monolix is required to run the code. All features are then analyzed using data science techniques (preprocess, feature selection, machine learning algorithms), in order to predict survival.

This demonstration code contains a git repository with step-by-step settings for performing each task.

# Directories architecture
- `code` : contains code and output directories and files
    - `overall_analysis` : scripts used to run the tasks
    - `overall.analysis` : *R* package containing functions used for performing the tasks
    - `pd_monolix` : directory used for NLME of the PD data with i) MLXTRAN versions of the dynamic models, ii) the `run_monolix_bayesian` function used for Bayesian estimation and iii) output files from the PD preprocess and NLME analyses
    - `post_process`: scripts and functions for post-process and outputs purposes
    - `TK_monolix` : directory used for NLME of the PD data with i) MLXTRAN versions of the dynamic models and ii) output files from the TK preprocess and NLME analyses
    - `clinical*` : directory used to store post-preprocess files of the clinical (baseline) data
    - `features_outcome*`: directory used to store and format feature sets and names of variables pertaining to signatures (i.e., subsets of all the variables) for use in ML phase
    - `rnaseq*`: directory used to store post-preprocess files of the RNAseq (baseline) data
- `./data*` :  raw data files
- `figures_tables` : R scripts generating the figures and tables in the paper

`*` indicates folders that are not present in the code-only version. 

ML = machine learning, NLME = nonlinear mixed-effects (modeling), PD = pharmacodynamic, TK = tumor kinetics