#______________________________________________________________________________
# settings ----
#______________________________________________________________________________
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
TK_param_dir   = "../../../TK_monolix/no_oak/"
BK_param_dir   = "../../../pd_monolix/no_oak/"
pop_param_path = "project/populationParameters.txt"
BK_features    = c("ldh", "crp", "neutrophils")
dir_out        = "../output"
#______________________________________________________________________________
# load pop parameters ----
#______________________________________________________________________________
TK_pop_param = read.table(
    "../../../TK_monolix/no_oak/project/populationParameters.txt",
    sep=',', 
    header = TRUE) %>%
    mutate(TK := sprintf(
        "%.3g (%.2f)",
        value,
        rse_sa
    )) %>%
    select(parameter, TK) %>%
    filter(!grepl("S0", parameter)) %>%
    mutate(parameter = dplyr::case_when(
        parameter == "a" ~ "error",
        TRUE ~ parameter
    ))

BK_pop_param = BK_features %>% 
purrr::map(.f = function(BK_feature){
    read.table(
        file.path(
            BK_param_dir,
            BK_feature,
            pop_param_path
            ),
        sep = ',', 
        header = TRUE) %>%
    mutate(!!BK_feature := sprintf(
        "%.3g (%.2f)",
        value,
        rse_sa
    )) %>%
    select(parameter, !!BK_feature) %>%
    filter(!grepl("S0", parameter))
}) %>% reduce(inner_join, by = "parameter") %>%
    mutate(parameter = dplyr::case_when(
        parameter == "b" ~ "error",
        TRUE ~ parameter
    ))

albumin_pop_param = read.table(
        file.path(
            BK_param_dir,
            "albumin",
            pop_param_path
            ),
        sep = ',', 
        header = TRUE) %>%
    mutate(albumin := sprintf(
        "%.3g (%.3g)",
        value,
        rse_sa
    )) %>%
    select(parameter_alb = parameter, albumin) %>%
    filter(!grepl("q", parameter_alb)) %>%
    mutate(parameter_alb = dplyr::case_when(
        parameter_alb == "b" ~ "error",
        TRUE ~ parameter_alb
    ))
 
TK_pop_param %>%
inner_join(BK_pop_param, by = "parameter") %>%
bind_cols(albumin_pop_param) %>%
select(parameter, TK, CRP = crp, LDH = ldh, Neutrophils = neutrophils, parameter_alb, Albumin = albumin) %>%
flextable::flextable() %>%
flextable::save_as_docx(path = file.path(
    dir_out,
    "table_1.docx"
))
