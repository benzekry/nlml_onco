# ______________________________________________________________________________
# settings ----
# ______________________________________________________________________________
library(tidyverse)
source("overall.analysis/R/survivalROC_ppv_npv.R")
source("overall.analysis/R/ROC_survival.R")
source("post_process/prettify_contingency.r")

results_minimal = readRDS(file.path(
  "machine_learning",
  "oak",
  "test_results",
  "random_survival_forest",
  "post_processed_results",
  "clinical_minimal_tk_no_select_pd_model_no_select.rds"
))

df_survival_test = readr::read_csv(file.path(
  "features_outcome",
  "oak",
  "survival.csv"
),
show_col_types = FALSE
)

df_survival_train = readr::read_csv(file.path(
  "features_outcome",
  "no_oak",
  "survival.csv"
),
show_col_types = FALSE
)

landmark_times        = 365
names(landmark_times) = "12_months"

tasks = list( # defines the cutpoint strategy
  ML_score_surv_prob = TRUE,
  ML_mortality       = FALSE
)

# _______________________________________________________________________________
# Defining optimal cutpoints ----
# _______________________________________________________________________________
## ML surv prob case: using train proba to define the optimal cutpoints ----
if (tasks$ML_score_surv_prob) {
  km_train <- survminer::surv_fit(survival::Surv(TIME, STATUS) ~ 1, data = df_survival_train)
  df_landmarks <- purrr::map_df(
    landmark_times,
    ~ tibble::tibble(
      time   = km_train$time,
      surv   = km_train$surv,
      low_95 = km_train$lower,
      up_95  = km_train$upper
    ) %>%
      dplyr::filter(abs(time - .x) == min(abs(time - .x))) %>%
      dplyr::slice(1)
  )
  opt_cutpoints <- 1 - df_landmarks$surv
  names(opt_cutpoints) <- landmark_times
}

# _______________________________________________________________________________
# ROC curves and contingency tables (minimal model) (each landmark time) ----
# _______________________________________________________________________________
## ML surv probability as score.
## Optimal cutpoint given by the observed train survival proba at landmark time

metrics = ROC_survival(
  time_             = landmark_time,
  survival_curves   = results_minimal$results_all,
  df_survival       = df_survival_test %>%
    dplyr::arrange(ID), # need to arrange by ID to have the same sorting for survival and mortality_scores
  only_metrics      = TRUE,
  opt_cutpoint_mode = "train_proba",
  opt_cutpoint      = opt_cutpoints[as.character(landmark_time)]
)

# prettify and save contingency table
# note: here 1 (or TRUE) stands for dead and 0 for alive
prettify_contingency(metrics$contingency_table) %>%
  flextable::save_as_pptx(path = "manuscript/figures_tables/output/table_2.pptx")
